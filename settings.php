  <?php
    	session_start();
      $UID = $_SESSION['id'];
      if (!isset($UID)) header("Location: log_in.php");
  ?>
  <?php
      function sql_q($field, $table, $condition) {
                  $link = mysqli_connect("localhost", "zl0ta", "ins3cwetr4st", "zl0ta_");
                  $stmt = mysqli_stmt_init($link);
                  $query= "SELECT `$field` FROM `$table` WHERE $condition";
                  $result = mysqli_query($link, $query) OR die(mysqli_error($link));
                  $count = mysqli_num_rows($result);
                  if ($count > 0) {
                    $row = mysqli_fetch_all($result, MYSQLI_ASSOC);
                    $str = json_encode($row);
                    $myArray = json_decode($str, true);
                    return $myArray[0][$field] ;
                  }
      }
      function sql_update($text, $field, $table,  $condition) {
                //$link = mysqli_connect("localhost", "zl0ta", "ins3cwetr4st", "zl0ta_");
                global $link;
                echo "$field $table $condition >>>>>>>>>>>>>>>>>";
                //$stmt = mysqli_stmt_init($link);
                $query= "UPDATE `$table` SET `$field`=\"$text\" WHERE $condition";
                $result = mysqli_query($link, $query) OR die(mysqli_error($link));
    }
      function sql_update_or_insert($list_val, $list_field, $table) {
                //$link = mysqli_connect("localhost", "zl0ta", "ins3cwetr4st", "zl0ta_");
                global $link;
                echo ">>>>>>>>>>>>>>>>> $list_val, $list_field, $table <<<<<<<<<<<<<<<<<";
                //$stmt = mysqli_stmt_init($link);
                $query= "REPLACE INTO `$table` (".$list_val.") VALUES (".$list_field.");";
                $result = mysqli_query($link, $query) OR die(mysqli_error($link));
    }
  ?>
  <script>
    function showStuff(id) {
            var el = document.getElementById(id);
            if (el.style.display === "none") {
              el.style.display = "block";
            } else {
              el.style.display = "none";
            }
        }
  </script>

  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8" />
    <title> Налаштування </title>
    <meta name="viewport" content="width=device-width, initial_scale=1.0">   <!--  адаптивность к мобильным устройствам-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="css/settings.css?v=2">
    <link rel="stylesheet" href="css/based.css">
  </head>
  <body style="width: 98%; margin: auto;">
    <?php echo file_get_contents("menu.php"); ?>
    <br>
    <div class="row">
      <!--фото-->
      <div class="col-md-3">
        <div class="row" style="margin-bottom: 8px;">
          <h3 style="align-self: center; margin: 0 auto;">
            Активний користувач:
          </h3>
          <h3 style="align-self: center; margin: 0 auto;">
            <?php echo "$UID - ".sql_q('name', 'u_data', 'id='.$UID); ?>
          </h3>
        </div>
        <div class="row" style="margin-bottom: 8px;">
          <img style="align-self: center; margin: 0 auto;" class="big_avka" src="../img/<?php echo "$UID"; ?>.jpg" onerror="this.src='img/default.png'">
        </div>
        <div class="row">
          <button class="btn btn-sm" onclick="showStuff('img_upload'); return false;" style="background-color: #ffb145; color: white; font-weight: bold; align-self: center; margin: 0 auto;">
            Змінити фото
          </button>
        </div>
        <br>
        <div class="row" style="background-color: #c5efee; align-self: center; margin: 0 auto 50px auto; padding: 15px; display:none;" id = "img_upload" >
          <form class="par-sett" action="php/img_upload_.php" method="post" id = "img_upload" enctype="multipart/form-data">
            Завантажте фото профілю:
            <input type="file" name="avka" id="avka"> <br>
            <input type="submit" value="Завантажити" name="submit">
          </form>
        </div>
      </div>

      <div class="col-md-5">
        <div class="col text-center">
          <!--про себе-->
          <div class="row">
            <h3 style="align-self: center; margin: 0 auto;">
              Про себе
            </h3>
          </div>
          <div class="row" style="padding: 15px; display:block;" id = "pro_sebya_form">
            <form style="background-color: #c5efee; align-self: center; margin: 0 auto; padding-top: 16px;" class="par-sett" action="php/settings_.php" method="post" autocomplete="on">
              <div class="row">
                <div class="col text-center">
                  <textarea rows="7" style="width:80%; margin-bottom: 8px;" maxlength = "2000" class = "pro_sebya_upd" name="pro_sebya_upd"  required placeholder="Новий текст про себе..." minlength="20"><?php echo sql_q('pro_sebya', 'pro_sebya', 'id='.$UID); ?></textarea>
                  <br>
                  <input type="submit" name="ok_pro_sebya_form" value= "Змінити" class="btn">
                </div>
              </div>
            </form>
          </div>

          <!--налаштування-->
          <div class="row">
            <h3 style="margin: 0 auto 16px auto;">
              Загальні налаштування
            </h3>
          </div>
          <div class="row">
            <form style="width:95%; background-color: #c5efee; align-self: center; margin: 0 auto; padding-top: 16px;" class="par-sett" action="php/settings_.php" method="post">
              <div class="row" id="personal_data">
                <table style="width: 75%; align-self: center; margin: 0 auto;">
                  <tr>
                    <td>Логін:</td>
                    <td><input type="text" name="login" value=<?php echo '"'.sql_q('login', 'u_data', 'id='.$UID).'"' ?>></td>
                    <td><input type="submit" name="ok1" value="Змінити" class="btn"></td>
                  </tr>
                  <tr>
                    <td>Ім'я користувача:</td>
                    <td><input type="text" name="name" value=<?php echo '"'.sql_q('name', 'u_data', 'id='.$UID).'"' ?>></td>
                    <td><input type="submit" name="ok2" value="Змінити" class="btn"></td>
                  </tr>
                  <tr>
                    <td>Пароль:</td>
                    <td><input type="password" name="password"></td>
                    <td><input type="submit" name="ok3" value="Змінити" class="btn"></td>
                  </tr>
                </table>
            	</div>
            </form>
          </div>
        </div>
      </div>

      <!--контактне повідомлення-->
      <div class="col-md-4">
        <div class="col text-center">
          <div class="row">
            <h3 style="margin: 0 auto 16px auto;">
              Контактне повідомлення
            </h3>
            <p>
              Введіть сюди посилання на ваші аккаунти у інших мережах.
              Це повідомлення буде надіслано хелперу після прийняття вами його допомоги з вашою мрією.
              Не соромтеся знаходити нових друзів ;)
            </p>
          </div>
          <div class="row" style="padding: 15px; display:block;" id = "ok_contact_sms_form">
            <form style="background-color: #c5efee; align-self: center; margin: 0 auto; padding-top: 16px;" class="par-sett" action="php/settings_.php" method="post" autocomplete="on">
              <div class="row">
                <div class="col text-center">
                  <textarea rows="7" style="width:80%; margin-bottom: 8px;" maxlength = "150" class = "contact_sms_upd" name="contact_sms_upd"  required placeholder="Новий текст про себе..."><?php echo sql_q('contact_sms', 'u_data', 'id='.$UID); ?></textarea>
                  <br>
                  <input type="submit" name="ok_contact_sms_form" value= "Змінити" class="btn">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
  </html>
