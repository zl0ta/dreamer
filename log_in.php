<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
  <title> Вхід </title>
  <meta name="viewport" content="width=device-width, initial_scale=1.0">   <!-- адаптивность к мобильным устройствам -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
	<link rel="stylesheet" href="css/based.css?v=3">
	<link rel="stylesheet" href="css/log_reg.css?v=4">
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 reg">
				<form style="padding-bottom: 12px;" action="php/log_in_.php" method="post">
					<span class="text"> Логін: </span><br>
					<input name="login" type="text"><br>
					<span class="text"> Пароль: </span><br>
					<input type="Password" name="password" id="Password" autocomplete="off"><br>
					<input type="submit" name="log_in" value="Увійти" class="text btn btn-warning btn-lg" style="background-color: #ffb145; color: white; font-size: 24px; font-weight: bold;">
				</form>
				<div class="button">
					<button class="text btn btn-warning btn-lg" style="background-color: #ffb145; color: white;" onclick="window.location.href='reg.php'">
						Реєстрація
					</button>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</body>
</html>
