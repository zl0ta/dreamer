-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Янв 22 2020 г., 01:13
-- Версия сервера: 10.4.10-MariaDB
-- Версия PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zl0ta_`
--

-- --------------------------------------------------------

--
-- Структура таблицы `pro_sebya`
--

CREATE TABLE `pro_sebya` (
  `id` int(11) NOT NULL,
  `pro_sebya` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `pro_sebya`
--

INSERT INTO `pro_sebya` (`id`, `pro_sebya`) VALUES
(1, 'Звичайна дівчина Злата'),
(21, 'It is me'),
(15, 'Григо́рій Са́вич Сковорода́ (22 листопада (3 грудня) 1722, Чорнухи, Лубенський полк — 29 жовтня (9 листопада) 1794, Іванівка, Харківщина) — видатний український філософ-містик, богослов, поет, педагог, можливо, і композитор літургійної музики. Мав значний вплив на сучасників і подальші покоління своїми байками, піснями, філософськими творами, а також способом життя, через що його називали «Сократом».');

-- --------------------------------------------------------

--
-- Структура таблицы `u_data`
--

CREATE TABLE `u_data` (
  `id` int(11) NOT NULL,
  `login` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_date` int(10) UNSIGNED NOT NULL,
  `help_wishes_done` int(11) DEFAULT 0,
  `used_help_points` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `u_data`
--

INSERT INTO `u_data` (`id`, `login`, `name`, `password`, `reg_date`, `help_wishes_done`, `used_help_points`) VALUES
(1, 'zlata.ranch91@gmail.com', 'Злота', '$2y$10$N5lovIyAwEfoC21V0M9ePOws4aGG6/pv5SLvhQTGf0MYymEJjj8QK', 1576262686, 0, 0),
(2, 'nikitabas83@gmail.com', 'Акіменко', '$2y$10$PXD/WPZPW4ppykXj8u7qJuUwWSmGfjU4JF04ixInLcWHoWSC4ZnZa', 1576264473, 0, 0),
(3, 'yatskovaiya2004@gmail.com ', 'Ия', '$2y$10$ApZHZWRM7DHXfmvEzKFRgOMBofKWSvjGr2ThIpwB8YCSnZtt1vfha', 1576264752, 0, 0),
(4, 'mihanamaster13@gmail.com', 'Michael', '$2y$10$Q2LIItBwGbptHG57oyvUD.8erVPFqWsy0gPrVKMOOfG9o9iQLpFgS', 1576268376, 0, 0),
(12, 'Zl0ta@gmail.com', 'test', '$2y$10$IV6jhAS/yxpEvaFCYqmdouUrATBsZEmG7kc7QOw6YPNVQv80YKBgi', 1576344980, 0, 0),
(13, '222', 'test', '$2y$10$bC0BncohnKMBlEEnMAHjBuxfoCKyv7bnm5lSrewfo0YXkz4lOeHW2', 1576366051, 0, 0),
(14, '111', '111', '$2y$10$2/KmG.TVnbM9jhIbOspIrOm4qVig4p1NL.OZuF9ID7R3agHWCPcmq', 1576369410, 0, 0),
(15, '1', 'Григо́рій Са́вич Сковорода́', '$2y$10$yTfo/Wu0g1koStt.PP.dwuFpgdsikC1dHBat0j6yvV./I12w2Yoyy', 1576625865, 7, 2),
(18, '2', '2', '$2y$10$YAybz48qfRCVacEnhtNbQuJUtxgUqALEJsKRHnEC0KrCXKjmnFM1G', 1576626148, 0, 0),
(19, '3', '3', '$2y$10$N5lovIyAwEfoC21V0M9ePOws4aGG6/pv5SLvhQTGf0MYymEJjj8QK', 1576626172, 0, 0),
(20, '4', '4', '$2y$10$kqGo6Yh/fYK11OH4huJHWOzog1KhfCjikxz9upMP1K4Od5AqovArG', 1576638754, 0, 0),
(21, 'qaz', 'qaz', '$2y$10$ECGkDVAWzXc.bRpGFnrgT.Iwq3BfbCQgtAgfIWJJyXiAVg1CoTrmm', 1578779814, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wishes`
--

CREATE TABLE `wishes` (
  `id` int(11) NOT NULL,
  `wish_id` int(11) NOT NULL,
  `wish_text` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wish_status` int(11) NOT NULL DEFAULT 0 COMMENT '0-100 - процент виконання'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wishes`
--

INSERT INTO `wishes` (`id`, `wish_id`, `wish_text`, `wish_status`) VALUES
(9, 0, 'Тестове бажання №2', 0),
(15, 1, 'Хочу щоб всі були вільними', 77),
(15, 0, 'Хочу бути вільним', 100),
(21, 0, '1111111 111 111 111111', 0),
(21, 1, '222 22 222222 222 22', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `pro_sebya`
--
ALTER TABLE `pro_sebya`
  ADD UNIQUE KEY `id` (`id`,`pro_sebya`) USING HASH;

--
-- Индексы таблицы `u_data`
--
ALTER TABLE `u_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `u_data`
--
ALTER TABLE `u_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
