<?php
  session_start();
  $UID = $_SESSION['id'];
  if (!isset($UID)) header("Location: log_in.php");

  require 'php/index_.php';
?>

<!DOCTYPE html>
<html>
<head>
  <!-- Site made with Mobirise Website Builder v4.3.5, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Мої дрімери </title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="css/based.css?v=3">
</head>

<body style="width: 98%; margin: auto;">
  <?php echo file_get_contents("menu.php"); ?>

  <!---заголовок--->
  <div class="row">
    <div class="col-md-12">
      <h3 class="text-center">
        Збережене
      </h3>
    </div>
  </div>
  <!---сохры--->
  <div class="container-fluid">
    <?php
    $wish_count = sql_count('wish_id', 'wishes', 'id='.$UID."  " );
    echo '<div class="card-columns">';
    for ($i =0 ; $i < $wish_count ; $i++) {
      $wish_status = sql_q('wish_status', 'wishes', 'id='.$UID." and wish_id=".$i );
      if ($wish_status<100) {
        show_wish($UID,$i);
      }
    };
    echo '</div>';
    ?>
  </div>

  <!---заголовок--->
  <div class="row">
    <div class="col-md-12">
      <h3 class="text-center">
        Контакти
      </h3>
    </div>
  </div>
  <!---контакты дримера + его желание--->
  <div class="container-fluid">
    <?php
    $wish_count = sql_count('wish_id', 'wishes', 'id='.$UID."  " );
    echo '<div class="card-columns">';
    for ($i = 0 ; $i < $wish_count ; $i++) {
      $wish_status = sql_q('wish_status', 'wishes', 'id='.$UID." and wish_id=".$i );
      if ($wish_status<100) {
        show_wish($UID,$i);
      }
    };
    echo '</div>';
    ?>
  </div>

  <!---неподтвержденные желания--->
  <div class="row">
    <div class="col-md-12">
      <h3 class="text-center">
        Збережене
      </h3>
    </div>
  </div>
  <!---заголовок--->
  <div class="container-fluid">
    <?php
    $wish_count = sql_count('wish_id', 'wishes', 'id='.$UID."  ");
    echo '<div class="card-columns">';
    for ($i = 0 ; $i < $wish_count ; $i++) {
      $wish_status = sql_q('wish_status', 'wishes', 'id='.$UID." and wish_id=".$i );
      if ($wish_status<100) {
        show_wish($UID,$i);
      }
    };
    echo '</div>';
    ?>
  </div>
</body>
