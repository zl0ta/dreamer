<?php
  session_start();
  $UID = $_SESSION['id'];
  echo "$UID";
  $link = mysqli_connect("localhost", "zl0ta", "ins3cwetr4st", "zl0ta_");

  //перевірка під'єднання до БД
  if (!$link) {
    echo "Помилка: неможливо встановити з'єднання з MySQL." . PHP_EOL;
    echo "Код помилки errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Текст помилки error: " . mysqli_connect_error() . PHP_EOL;
    exit;
  }

    //відправляємо запит до БД
  $link->query("SET NAMES 'utf8'");

  //1-а кнопка
  if (isset($_POST["ok1"])) {
    $stmt = mysqli_stmt_init($link);
    if (mysqli_stmt_prepare($stmt, "UPDATE `u_data` SET `login` = ? WHERE `id`= ?")) {
      mysqli_stmt_bind_param($stmt, "ss", $login, $UID);
      $login = $_POST['login'];
      if (mysqli_stmt_execute($stmt)) {
        echo "<script>window.location.href='/settings.php';</script>";
      }
    }
    mysqli_stmt_close($stmt);
  }


  //2-а кнопка
  if (isset($_POST["ok2"])) {
    $stmt = mysqli_stmt_init($link);
    if (mysqli_stmt_prepare($stmt, "UPDATE `u_data` SET `name` = ? WHERE `id`= ?")) {
      mysqli_stmt_bind_param($stmt, "ss", $name, $UID);
      $name = $_POST['name'];
      if (mysqli_stmt_execute($stmt)) {
        echo "<script>window.location.href='/settings.php';</script>";
      }
      mysqli_stmt_close($stmt);
    }
  }

  //3-я кнопка
  if (isset($_POST["ok3"])) {
    $stmt = mysqli_stmt_init($link);
    if (mysqli_stmt_prepare($stmt, "UPDATE `u_data` SET `password` = ? WHERE `id`= ?")) {
      mysqli_stmt_bind_param($stmt, "ss", $password, $UID);
      $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
      if (mysqli_stmt_execute($stmt)) {
        echo "Пароль успішно змінено!";
        echo "<script>window.location.href='/settings.php';</script>";
      }
      mysqli_stmt_close($stmt);
    }
  }

  require 'index_.php';
  //ok_w_new - кнопка
  /*а этот кусок вообще теперь работает?*/
  if (isset($_POST["ok_w_new"])) {
    $stmt = mysqli_stmt_init($link);
    $wish_text = $_POST['wishes_new'];
    $wish_count = sql_count('wish_id', 'wishes', 'id='.$UID." " );

        echo $wish_count;
        if ($wish_count <=2) { // TODO: проверка кол-ва разрешенных мечт. Сейчас меньше 3-х
          if (mysqli_stmt_prepare($stmt, "INSERT INTO `wishes` (`wish_text`, `id`, `wish_id`, `wish_status`) VALUES ( ? , ? , ?, 0) " )) {
            mysqli_stmt_bind_param($stmt, "sdd", $wish_text, $UID, $wish_count);
            if (mysqli_stmt_execute($stmt)) {
              ?> <script type="text/javascript"> window.alert("Мрія доданаі");  </script> <?php

            }
          }

      mysqli_stmt_close($stmt);

    } else {
            ?> <script type="text/javascript"> window.alert("Нажаль ви ще не можите додати мрію. Спочатку вам треба допомогти іншим здійснити їх мрії."); </script> <?php
          }
    echo "<script>window.location.href='/index.php';</script>";
  }

  //ok_pro_sebya_form - кнопка
  if (isset($_POST["ok_pro_sebya_form"])) {
    $stmt = mysqli_stmt_init($link);
    $pro_sebya_txt = $_POST['pro_sebya_upd'];
    //echo "$pro_sebya_txt";
    $text_count = sql_count('pro_sebya', 'pro_sebya', 'id='.$UID." " );
    if ($text_count >0 ){
    	sql_update($pro_sebya_txt, "pro_sebya", "pro_sebya", "`id`=".$UID);
    } else {
    	    sql_update_or_insert("`id`, pro_sebya", " ".$UID.", "."\"$pro_sebya_txt\"",  "pro_sebya");

    	} 
    echo "<script>window.location.href='/settings.php';</script>";
  }

  //ok_contact_sms_form - кнопка
  if (isset($_POST["ok_contact_sms_form"])) {
    $stmt = mysqli_stmt_init($link);
    $contact_sms_txt = $_POST['contact_sms_upd'];
    //echo "$contact_sms_txt";
    sql_update($contact_sms_txt, "contact_sms", "u_data", "`id`=".$UID);
    echo "<script>window.location.href='/settings.php';</script>";
  }


  //ok_w1-я кнопка
  if (isset($_POST["ok_w1"])) {
    $stmt = mysqli_stmt_init($link);
    $wish_text = $_POST['wishes_1'];
    if (mysqli_stmt_prepare($stmt, "UPDATE `wishes` SET `wish_text` = ? WHERE `id`= ? and `wish_id`= 0" )) {
      mysqli_stmt_bind_param($stmt, "sd", $wish_text, $UID);
      if (mysqli_stmt_execute($stmt)) {
        echo "<script>window.location.href='/settings.php';</script>";
        if (mysqli_stmt_affected_rows($stmt)<=0) {
          if (mysqli_stmt_prepare($stmt, "INSERT INTO `wishes` (`wish_text`, `id`, `wish_id`, wish_status) VALUES ( ? , ? , 0, 0) " )) {
            mysqli_stmt_bind_param($stmt, "sd", $wish_text, $UID);
            if (mysqli_stmt_execute($stmt)) {
              echo "<script>window.location.href='/settings.php';</script>";
            }
          }
        }
      }
      mysqli_stmt_close($stmt);
    }
  }

    //ok_w2-я кнопка
  if (isset($_POST["ok_w2"])) {
    $stmt = mysqli_stmt_init($link);
    $wish_text = $_POST['wishes_2'];
    if (mysqli_stmt_prepare($stmt, "UPDATE `wishes` SET `wish_text` = ? WHERE `id`= ? and `wish_id`= 1" )) {
      mysqli_stmt_bind_param($stmt, "sd", $wish_text, $UID);
      if (mysqli_stmt_execute($stmt)) {
        echo "<script>window.location.href='/settings.php';</script>";
        if (mysqli_stmt_affected_rows($stmt)<=0) {
          if (mysqli_stmt_prepare($stmt, "INSERT INTO `wishes` (`wish_text`, `id`, `wish_id`, wish_status) VALUES ( ? , ? , 1, 0) " )) {
            mysqli_stmt_bind_param($stmt, "sd", $wish_text, $UID);
            if (mysqli_stmt_execute($stmt)) {
              echo "<script>window.location.href='/settings.php';</script>";
            }
          }
        }
      }
      mysqli_stmt_close($stmt);
    }
  }

      //ok_w3-я кнопка
  if (isset($_POST["ok_w3"])) {
    $stmt = mysqli_stmt_init($link);
    $wish_text = $_POST['wishes_3'];
    if (mysqli_stmt_prepare($stmt, "UPDATE `wishes` SET `wish_text` = ? WHERE `id`= ? and `wish_id`= 2" )) {
      mysqli_stmt_bind_param($stmt, "sd", $wish_text, $UID);
      if (mysqli_stmt_execute($stmt)) {
        echo "<script>window.location.href='/settings.php';</script>";
        if (mysqli_stmt_affected_rows($stmt)<=0) {
          if (mysqli_stmt_prepare($stmt, "INSERT INTO `wishes` (`wish_text`, `id`, `wish_id`, wish_status) VALUES ( ? , ? , 2, 0) " )) {
            mysqli_stmt_bind_param($stmt, "sd", $wish_text, $UID);
            if (mysqli_stmt_execute($stmt)) {
              echo "<script>window.location.href='/settings.php';</script>";
            }
          }
        }
      }
      mysqli_stmt_close($stmt);
    }
  }

  $link->close();
?>
