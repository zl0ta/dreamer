<?php
  session_start();
  $UID = $_SESSION['id'];
  if (!isset($UID)) header("Location: log_in.php");

  require 'php/index_.php';
?>

<!DOCTYPE html>
<html>
<head>
  <!-- Site made with Mobirise Website Builder v4.3.5, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Мій профіль </title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="css/based.css?v=3">
</head>

<body style="width: 98%; margin: auto;">
  <?php echo file_get_contents("menu.php"); ?>
  <div class="container-fluid">
    <div class="row" style="background-color: #c5efee;">
      <div class="col-md-12">
        <div class="row">
          <!---AVKA--->
          <div class="col-md-3" align="center" style="margin-top: 20px;">
            <img class="big_avka" src="img/<?php echo "$UID";?>.jpg" onerror="this.src='img/default.png'" class="rounded-circle" />
            <!---айді--->
            <h3 class="text-center">
              <?php echo "$UID - $name";?>
            </h3>
            <a href="/settings.php" role="button" class="btn btn-link" style="margin-left: -13px;">Налаштування</a>
          </div>

          <!--STATICS-->
          <div class="col-md-9" style="margin-top: 26px;" align="center">
            <div class="row">
              <!---ICON1--->
              <div class="col-md-3 cell-center">
                <div class="row ">
                  <div class="col-md-2">
                    <H1 class="btn disabled font-weight-bold btn-lg num_icon">
                      <?php
                      $help_wishes_done = sql_q('help_wishes_done', 'u_data', 'id='.$UID." " );
                      echo $help_wishes_done ;
                      ?>
                    </H1>
                  </div>
                  <div class="col-md-7 cell-center">
                    <h5>
                      Допоміг людям
                    </h5>
                  </div>
                </div>
              </div>
              <!---ICON2--->
              <div class="col-md-3 cell-center">
                <div class="row">
                  <div class="col-md-2">
                    <H1 class="btn disabled font-weight-bold btn-lg num_icon">
                      <?php
                      $wish_count = sql_count('wish_id', 'wishes', 'id='.$UID." and  wish_status = \"100\" " );
                      echo $wish_count ;
                      ?>
                    </H1>
                  </div>
                  <div class="col-md-8 cell-center">
                    <h5>
                      Бажань виконано
                    </h5>
                  </div>
                </div>
              </div>
              <!---ICON3--->
              <div class="col-md-3 cell-center">
                <div class="row">
                  <div class="col-md-2">
                    <H1 class="btn disabled font-weight-bold btn-lg num_icon">
                      <?php
                      $wish_count = sql_count('wish_id', 'wishes', 'id='.$UID." and  wish_status != \"100\" " );
                      echo $wish_count ;
                      ?>
                    </H1>
                  </div>
                  <div class="col-md-8 cell-left">
                    <h5>
                      Мрій в роботі
                    </h5>
                  </div>
                </div>
              </div>
              <!---ICON4--->
              <div class="col-md-3 cell-center">
                <div>
                  <div class="row cell-center">
                    <h5>
                      Дата реєстрації
                    </h5>
                  </div>
                  <div class="row cell-center">
                    <span class="text-center">
                      <?php echo date('d.m.Y', sql_q('reg_date', 'u_data', 'id='.$UID )) ?>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row"><br></div>

            <!---ABOUT ME--->
            <div class="row mr-1" align="left">
              <p>
                <strong>Про мене:</strong><br>
                <?php echo sql_q('pro_sebya', 'pro_sebya', 'id='.$UID ) ?>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row" style="background-color: white;"><br></div>

    <!---блок "додати бажання"--->
    <div class="row" style="background-color: #c5efee; padding: 16px; margin-bottom: 16px;">
      <div class="col-md-3">
        <div class="row">
          <div class="col-md-2 cell-center">
            <H1 class="btn btn-lg font-weight-bold disabled num_icon">
              <?php
              $help_wishes_done = sql_q('help_wishes_done', 'u_data', 'id='.$UID." " );
              $used_help_points = sql_q('used_help_points', 'u_data', 'id='.$UID." " );
              $wish_point = (int)(($help_wishes_done  - $used_help_points)/3);
              echo $wish_point;
              ?>
            </H1>
          </div>
          <div class="col-md-10 cell-center">
            <p>
              Бажань можна замріяти
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="row">
          <div class="col-md-2 cell-center">
            <H1 class="btn btn-lg disabled font-weight-bold num_icon">
              <?php
              $help_wishes_done = sql_q('help_wishes_done', 'u_data', 'id='.$UID." " );
              echo $help_wishes_done ;
              ?>
            </H1>
          </div>
          <div class="col-md-10 cell-center">
            <p>
              Разів допоміг(ла) іншим бажанням збутися
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="row">
          <div class="col-md-2 cell-center">
            <H1 class="btn btn-lg disabled font-weight-bold num_icon">
              <?php
              $help_wishes_done = sql_q('help_wishes_done', 'u_data', 'id='.$UID." " );
              $used_help_points = sql_q('used_help_points', 'u_data', 'id='.$UID." " );
              $help_point =  $help_wishes_done  - $used_help_points;
              if ($help_point >=3) {
                echo "0";
              } else {
                echo 3-$help_point ;
              }
              ?>
            </H1>
          </div>
          <div class="col-md-10 cell-center">
            <p>
              Скільки треба зробити до наступного бажання
            </p>
          </div>
        </div>
      </div>
      <!---Add mriya--->
      <div class="col-md-3 align-self-center">
        <H1 class="btn btn-warning btn-lg btn-block font-weight-bold" style="background-color: #ffb145; color: white" onclick="showStuff('mriya_form'); return false;" >
          Додати мрію
        </H1>
      </div>
    </div>

    <!---Add mriya hiden fields--->
    <div class="row" style="background-color: #c5efee; margin-bottom: 50px; padding: 15px; display:none;" id = "mriya_form" >
      <div class="col-md-5">
        <?php show_wish_img($UID, 999); ?>
      </div>
      <div class="col-md-7">
        <form action="php/settings_.php" method="post" autocomplete="on" >
          <textarea rows="10" style="width:80%;" maxlength = "2000" class = "wishes_input" name="wishes_new"  required placeholder="Створить мрію..." minlength="40"></textarea>
          <br/>
          <input type="submit" name="ok_w_new" value="Додати" class="btn btn-lg font-weight-bold" style="background-color: #ffb145; color: white" >
        </form>
      </div>

    </div>

    <!---теперішні бажання--->
    <?php
    $wish_count = sql_count('wish_id', 'wishes', 'id='.$UID."  " );
    echo '<div class="card-columns">';
    for ($i =0 ; $i < $wish_count ; $i++) {
      $wish_status = sql_q('wish_status', 'wishes', 'id='.$UID." and wish_id=".$i );
      if ($wish_status<100) {
        show_wish($UID,$i);
      }
    };
    echo '</div>';
    ?>

    <!---заголовок--->
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-center">
          Реалізовані бажання
        </h3>
      </div>
    </div>

    <!---реалізовані бажання--->
    <?php
    $wish_count = sql_count('wish_id', 'wishes', 'id='.$UID."  " );
    for ($i =0 ; $i < $wish_count ; $i++) {
      $wish_status = sql_q('wish_status', 'wishes', 'id='.$UID." and wish_id=".$i );
      if ($wish_status>=100) {
        show_done_wish($UID,$i);
      }
    };
    ?>
  </div>
</body>
</html>
