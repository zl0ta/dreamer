<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title> Реєстрація </title>
  <meta name="viewport" content="width=device-width, initial_scale=1.0"> <!-- адаптивність до мобільних пристороїв -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="css/based.css?v=2">
  <link rel="stylesheet" href="css/log_reg.css?v=2">
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4"></div>
        <div class="col-md-4 reg">
          <form action="php/reg_.php" method="post">
            <span class="text"> Логін: </span><br>
            <input type="text" name="login"><br>
            <span class="text"> Ваше ім'я: </span><br>
            <input type="text" name="name"><br>
            <span class="text"> Пароль: </span><br>
            <input type="password" name="password"><br>
            <input type="submit" value="Зареєструватися" class="text btn btn-warning btn-lg" style="background-color: #ffb145; color: white; font-size: 24px; font-weight: bold;">
          </form>
          <div class="button">
            <button class="text btn btn-warning btn-lg" style="background-color: #ffb145; color: white;" onclick="window.location.href='/log_in.php'">
              Вхід
            </button>
          </div>
        </div>
      <div class="col-md-4"></div>
  </div>
  </div>
</body>
</html>
