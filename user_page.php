<?php
  	session_start();
    $UID = $_SESSION['id'];
    if (!isset($UID)) header("Location: log_in.php");

    require 'php/user_page_.php';
?>

<!DOCTYPE html>
<html>
<head>
  <title><?php echo "$user_id";?></title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial_scale=1.0">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="css/user.css">
  <link rel="stylesheet" href="css/based.css">
</head>

<body style="width: 98%; margin: 0 auto;"> 
  <?php echo file_get_contents("menu.php"); ?>
  <?php
    /*if (!empty($_GET["user_id"])) {
      $user_id = $_GET["user_id"];
    } else echo "NOT OK!";*/
  ?>

  <div class="container-fluid">
  	<div class="row" style="background-color: #c5efee;">
  		<div class="col-md-12">
  			<div class="row">
  				<!---AVKA--->
          <div class="col-md-3" align="center" style="margin-top: 20px;">
  					<img class="big_avka" src="img/<?php echo "$user_id"; ?>.jpg" onerror="this.src='img/default.png'" class="rounded-circle" />
            <!---айді--->
            <h3 class="text-center">
              <?php echo sql_q('name', 'u_data', 'id='.$user_id) ?>
  					</h3>
  				</div>

          <!--STATICS-->
          <div class="col-md-9" style="margin-top: 26px;" align="center">
      			<div class="row">
      				<!---ICON1--->
              <div class="col-md-3 cell-center">
        				<div class="row ">
        					<div class="col-md-2">
                    <H1 class="btn disabled font-weight-bold btn-lg num_icon">
                      <?php
                        $help_wishes_done = sql_q('help_wishes_done', 'u_data', 'id='.$user_id." " );
                        echo $help_wishes_done ;
                      ?>
                    </H1>
        					</div>
        				  <div class="col-md-7 cell-center">
        						<h5>
        							Допоміг людям
        						</h5>
        					</div>
        				</div>
        			</div>
              <!---ICON2--->
        			<div class="col-md-3 cell-center">
        				<div class="row">
        					<div class="col-md-2">
      							<H1 class="btn disabled font-weight-bold btn-lg num_icon">
                      <?php
        				        $wish_count = sql_count('wish_id', 'wishes', 'id='.$user_id." and  wish_status = \"100\" " );
                        echo $wish_count ;
                      ?>
        						</H1>
        					</div>
        					<div class="col-md-8 cell-center">
        						<h5>
        							Бажань виконано
        						</h5>
      						</div>
      					</div>
      				</div>
              <!---ICON3--->
        			<div class="col-md-3 cell-center">
        				<div class="row">
        					<div class="col-md-2">
        						<H1 class="btn disabled font-weight-bold btn-lg num_icon">
        							<?php
                        $wish_count = sql_count('wish_id', 'wishes', 'id='.$user_id." and  wish_status != \"100\" " );
                        echo $wish_count ;
                      ?>
        						</H1>
        					</div>
        					<div class="col-md-8 cell-left">
      							<h5>
      								Мрій в роботі
      							</h5>
      						</div>
        				</div>
        			</div>
              <!---ICON4--->
        			<div class="col-md-3 cell-center">
        				<div>
                  <div class="row cell-center">
        						<h5>
        							Дата реєстрації
        						</h5>
        					</div>
                  <div class="row cell-center">
        						<span class="text-center">
                      <?php echo date('d.m.Y', sql_q('reg_date', 'u_data', 'id='.$user_id )) ?>
                    </span>
        					</div>
        				</div>
        			</div>
        		</div>

            <div class="row"><br></div>

            <!---ABOUT ME--->
            <div class="row mr-1" align="left">
    					<p>
    						<strong>Про мене:</strong><br>
                <?php echo sql_q('pro_sebya', 'pro_sebya', 'id='.$user_id ) ?>
  						</p>
            </div>
      		</div>
  			</div>
  		</div>
  	</div>

    <br>

    <!---блок "додати бажання"--->
    <div class="row" style="background-color: #c5efee; padding-top: 16px; margin-bottom: 16px;">
      <div class="col-md-2 align-self-center">
      </div>
      <div class="col-md-3">
        <div class="row">
          <div class="col-md-2">
            <H1 class="btn btn-lg font-weight-bold disabled num_icon">
              <?php
                $help_wishes_done = sql_q('help_wishes_done', 'u_data', 'id='.$user_id." " );
                $used_help_points = sql_q('used_help_points', 'u_data', 'id='.$user_id." " );
                $wish_point = (int)(($help_wishes_done  - $used_help_points)/3);
                echo $wish_point;
              ?>
            </H1>
          </div>
          <div class="col-md-10 cell-center">
            <p>
              Бажань можна замріяти
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="row">
          <div class="col-md-2">
            <H1 class="btn btn-lg disabled font-weight-bold num_icon">
              <?php
                $help_wishes_done = sql_q('help_wishes_done', 'u_data', 'id='.$user_id." " );
                echo $help_wishes_done ;
              ?>
            </H1>
          </div>
          <div class="col-md-10 cell-center">
            <p>
              Разів допоміг(ла) іншим бажанням збутися
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="row">
          <div class="col-md-2">
            <H1 class="btn btn-lg disabled font-weight-bold num_icon">
              <?php
                $help_wishes_done = sql_q('help_wishes_done', 'u_data', 'id='.$user_id." " );
                $used_help_points = sql_q('used_help_points', 'u_data', 'id='.$user_id." " );
                $help_point =  $help_wishes_done  - $used_help_points;
                if ($help_point >=3) {
                  echo "0";
                } else {
                  echo 3-$help_point ;
                }
              ?>
            </H1>
          </div>
          <div class="col-md-10 cell-center">
            <p>
              Скільки треба зробити до наступного бажання
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-1 align-self-center">
        
      </div>
    </div>


    <!---теперішні бажання--->
    <?php
      $wish_count = sql_count('wish_id', 'wishes', 'id='.$user_id."  " );
      echo '<div class="card-columns">';
        for ($i =0 ; $i < $wish_count ; $i++) {
          $wish_status = sql_q('wish_status', 'wishes', 'id='.$user_id." and wish_id=".$i );
          if ($wish_status<100) {
            show_wish($user_id,$i);
          }
        };
      echo '</div>';
    ?>

  	<!---заголовок--->
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-center">
          Реалізовані бажання
    		</h3>
      </div>
    </div>

    <!---реалізовані бажання--->
    <?php
      $wish_count = sql_count('wish_id', 'wishes', 'id='.$user_id."  " );
      for ($i =0 ; $i < $wish_count ; $i++) {
        $wish_status = sql_q('wish_status', 'wishes', 'id='.$user_id." and wish_id=".$i );
        if ($wish_status>=100) {
          show_done_wish($user_id,$i);
        }
      };
    ?>
  </div>
</body>
</html>
