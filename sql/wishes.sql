-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Фев 10 2020 г., 02:44
-- Версия сервера: 10.4.10-MariaDB
-- Версия PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zl0ta_`
--

-- --------------------------------------------------------

--
-- Структура таблицы `wishes`
--

CREATE TABLE `wishes` (
  `id` int(11) NOT NULL,
  `wish_id` int(11) NOT NULL,
  `wish_text` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wish_status` int(11) NOT NULL DEFAULT 0 COMMENT '0-100 - процент виконання',
  `add_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wishes`
--

INSERT INTO `wishes` (`id`, `wish_id`, `wish_text`, `wish_status`, `add_date`) VALUES
(9, 0, 'Тестове бажання №2', 0, '2020-02-02 11:21:37'),
(10, 0, 'ddasdasd', 0, '2020-02-02 11:21:37'),
(10, 1, 'asdasdas', 0, '2020-02-02 11:21:37'),
(10, 2, 'adasd asdasda adas da', 0, '2020-02-02 11:21:37'),
(1, 0, 'nongoer vrjnerjbnro rkntrklnrklr rejknrjnerngel erjkgnerjknerjk \r\nljbntbtrkbnl jbrjkhjfrbr tbkgnbrjk tr rjkbntrj rbjbrjnrivjd;oczn cjkbfdhgerufejiowv frt ujkyijotyhm nrjtiiorbn \n \n nongoer vrjnerjbnro rkntrklnrklr rejknrjnerngel erjkgnerjknerjk \r\nljbntbtrkbnl jbrjkhjfrbr tbkgnbrjk tr rjkbntrj rbjbrjnrivjd;oczn cjkbfdhgerufejiowv frt ujkyijotyhm nrjtiiorbn', 0, '2020-02-02 11:21:37'),
(1, 1, 'Овоуткт', 0, '2020-02-02 11:21:37'),
(15, 1, 'Хочу щоб всі були вільними', 77, '2020-02-02 11:21:37'),
(15, 0, 'Хочу бути вільним', 100, '2000-01-01 09:06:00'),
(1, 2, '\r\n         Я хочу придбати якусь якісну і цікаву книжку з економіки. У кого є зайва?)', 0, '2020-02-07 17:15:11'),
(15, 2, 'Мрію про велике.\r\nХочу написати козака Енея.', 0, '2020-02-10 00:07:25'),
(15, 4, 'wwwwwwwwwwwwwwwwwwwwwwww\r\nwwwwwwwwwwwwwwwwwwwwwwwww\r\nwwwwwwwwwwwwwwwwww', 50, '2020-02-10 00:32:36');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
