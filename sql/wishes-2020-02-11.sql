-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Фев 11 2020 г., 13:58
-- Версия сервера: 10.4.10-MariaDB
-- Версия PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zl0ta_`
--

-- --------------------------------------------------------

--
-- Структура таблицы `wishes`
--

CREATE TABLE `wishes` (
  `id` int(11) NOT NULL,
  `wish_id` int(11) NOT NULL,
  `wish_text` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wish_status` int(11) NOT NULL DEFAULT 0 COMMENT '0-100 - процент виконання',
  `add_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wishes`
--

INSERT INTO `wishes` (`id`, `wish_id`, `wish_text`, `wish_status`, `add_date`) VALUES
(12, 0, 'Мечтаю переехать жить в Польшу. Для этого мне надо: 1) выучить язык; 2) предварительно поездить по разным городам Польши, чтобы выбрать тот, в котором захочется жить; 3) найти варианты работы; 4) нуждаюсь в консультациях по самым разным вопросам (документы, жильё, адаптация и т.п.).', 0, '2020-02-02 11:21:37'),
(14, 0, 'Мрію провести чарівний вечір з дівчиною, чимось її вразити. Вона любить природу і пригоди, а я не знаю, що такого незабутнього на природі можна влаштувати для неї. Порадьте і допоможіть!', 0, '2020-02-02 11:21:37'),
(1, 1, 'Овоуткт', 0, '2020-02-02 11:21:37'),
(3, 1, 'Хочу щоб всі були вільними', 77, '2020-02-02 11:21:37'),
(15, 0, 'Хочу помститися а вбитого сина - князя Ігоря.', 0, '2000-01-01 09:06:00'),
(16, 0, 'Хочу перемогти Хозарський каганат. ', 0, '2000-01-01 09:06:00'),
(17, 0, 'Мрію створити державну раду ', 0, '2000-01-01 09:06:00'),
(18, 0, 'Мрію написати Повісті временних літ ', 0, '2000-01-01 09:06:00'),
(19, 0, 'Мрію написати Руську Правду', 0, '2000-01-01 09:06:00'),
(19, 1, 'Мрію видати дочок за королів ', 0, '2000-01-01 09:06:00'),
(1, 0, '\r\n         Я хочу придбати якусь якісну і цікаву книжку з економіки. У кого є зайва?)', 0, '2020-02-07 17:15:11'),
(3, 2, 'Мрію про велике.\r\nХочу написати козака Енея.', 0, '2020-02-10 00:07:25'),
(3, 4, 'wwwwwwwwwwwwwwwwwwwwwwww\r\nwwwwwwwwwwwwwwwwwwwwwwwww\r\nwwwwwwwwwwwwwwwwww', 50, '2020-02-10 00:32:36'),
(17, 0, '<input type=\"hidden\" name=\"helper_id\" value=<?php echo \"$UID\"; ?> >\r\n                            <input type=\"hidden\" name=\"wish_id\" value=<?php echo \"$wish_id\"; ?> >\r\n                            <input type=\"hidden\" name=\"user_id\" value=<?php echo \"$user_id\"; ?> >', 0, '2020-02-10 11:08:51');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
