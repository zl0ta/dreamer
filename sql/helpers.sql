-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Фев 10 2020 г., 02:44
-- Версия сервера: 10.4.10-MariaDB
-- Версия PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zl0ta_`
--

-- --------------------------------------------------------

--
-- Структура таблицы `helpers`
--

CREATE TABLE `helpers` (
  `helper_id` int(11) NOT NULL,
  `wish_id` int(11) NOT NULL,
  `dreamer_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `helper_text` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `helpers`
--

INSERT INTO `helpers` (`helper_id`, `wish_id`, `dreamer_id`, `status`, `helper_text`) VALUES
(15, 1, 1, 1, ''),
(15, 2, 1, 1, 'Ще більш вмістовн допомога....'),
(17, 2, 15, 2, 'Вмістовна допомога для реалізації мрії'),
(17, 4, 15, 2, 'Вмістовна допомога для реалізації мрії'),
(17, 1, 15, 2, 'Вмістовна допомога для реалізації мрії');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
