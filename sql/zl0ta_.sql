-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Фев 10 2020 г., 02:44
-- Версия сервера: 10.4.10-MariaDB
-- Версия PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zl0ta_`
--

-- --------------------------------------------------------

--
-- Структура таблицы `helpers`
--

CREATE TABLE `helpers` (
  `helper_id` int(11) NOT NULL,
  `wish_id` int(11) NOT NULL,
  `dreamer_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `helper_text` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `helpers`
--

INSERT INTO `helpers` (`helper_id`, `wish_id`, `dreamer_id`, `status`, `helper_text`) VALUES
(15, 1, 1, 1, ''),
(15, 2, 1, 1, 'Ще більш вмістовн допомога....'),
(17, 2, 15, 2, 'Вмістовна допомога для реалізації мрії'),
(17, 4, 15, 2, 'Вмістовна допомога для реалізації мрії'),
(17, 1, 15, 2, 'Вмістовна допомога для реалізації мрії');

-- --------------------------------------------------------

--
-- Структура таблицы `pro_sebya`
--

CREATE TABLE `pro_sebya` (
  `id` int(11) NOT NULL,
  `pro_sebya` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `pro_sebya`
--

INSERT INTO `pro_sebya` (`id`, `pro_sebya`) VALUES
(1, 'Звичайна дівчина Злата'),
(21, 'It is me'),
(15, '');

-- --------------------------------------------------------

--
-- Структура таблицы `saved_wishes`
--

CREATE TABLE `saved_wishes` (
  `liker_id` int(11) NOT NULL,
  `wish_id` int(11) NOT NULL,
  `dreamer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `saved_wishes`
--

INSERT INTO `saved_wishes` (`liker_id`, `wish_id`, `dreamer_id`) VALUES
(11, 1, 2),
(15, 3, 15),
(15, 1, 15),
(17, 2, 15);

-- --------------------------------------------------------

--
-- Структура таблицы `u_data`
--

CREATE TABLE `u_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_date` int(10) UNSIGNED NOT NULL,
  `u_gave` int(11) NOT NULL,
  `all_wishes` int(11) NOT NULL,
  `curr_wishes` int(11) NOT NULL,
  `help_wishes_done` int(11) NOT NULL DEFAULT 0,
  `used_help_points` int(11) NOT NULL DEFAULT 0,
  `contact_sms` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `u_data`
--

INSERT INTO `u_data` (`id`, `login`, `name`, `password`, `reg_date`, `u_gave`, `all_wishes`, `curr_wishes`, `help_wishes_done`, `used_help_points`, `contact_sms`) VALUES
(1, 'zl0ta', 'zl0ta))', '$2y$10$Soy/XJcSUW7OEfWX45DLGeh.XoNQ55vOc2/64Zyb2jdia5ftaaOf6', 1577486719, 0, 0, 0, 0, 0, ''),
(2, 'tester_dtl', '111name', '$2y$10$o.HGlWkdYAnEQX5ZzIy7TOvK4RV.Shm53CZQlMJRvTxwjBcmlCrbi', 1578407710, 0, 0, 0, 0, 0, ''),
(3, 'yatskova_iya2004@gmail.com', 'Ія', '$2y$10$voC5ZfdFlB44DsgM0lh61eBcHZuI.cbvmR04D10nXtvGIyUwovH4y', 1578407850, 0, 0, 0, 0, 0, ''),
(4, 'mihanamaster13@gmail.com', 'Міша13', '$2y$10$n1rufx4d08NvXPWm/d2QMeJpbxZb9.dvV8BJCX7PxKRmWIqO4FIiu', 1578407922, 0, 0, 0, 0, 0, ''),
(5, 'Zlata_adm', 'Злата Ранчукова', '$2y$10$cQ6ozpnkdHjXaokbjRWy3eZ/7lILtHgUTrJjJX2/.iHCP/fujJ9.q', 1578570921, 0, 0, 0, 0, 0, ''),
(6, 'cuttlefish', 'Cuttle', '$2y$10$j9CY/zwQuthGYXryTy3Z4e0DTijKMiU2C/QhqRx9TuE0L1An/noe6', 1578589374, 0, 0, 0, 0, 0, ''),
(15, '1', 'Григо́рій Са́вич Сковорода́', '$2y$10$yTfo/Wu0g1koStt.PP.dwuFpgdsikC1dHBat0j6yvV./I12w2Yoyy', 1576625865, 0, 0, 0, 7, 2, 'acasas'),
(16, '333', '333', '$2y$10$6o4oG3qHuP.LQfQI1cXC8uRE/pVSb/CtesFIsNJ0owR9N.hmKkk9i', 1581287757, 0, 0, 0, 0, 0, ''),
(17, '321', '321', '$2y$10$Npb2I/Gse4HWCtFODkIvq.X2C.8Lo1shgT2ZJd8HThYyg0YTJg4jS', 1581290284, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `u_data_old`
--

CREATE TABLE `u_data_old` (
  `id` int(11) NOT NULL,
  `login` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_date` int(10) UNSIGNED NOT NULL,
  `help_wishes_done` int(11) DEFAULT 0,
  `used_help_points` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `u_data_old`
--

INSERT INTO `u_data_old` (`id`, `login`, `name`, `password`, `reg_date`, `help_wishes_done`, `used_help_points`) VALUES
(1, 'zlata.ranch91@gmail.com', 'Злота', '$2y$10$N5lovIyAwEfoC21V0M9ePOws4aGG6/pv5SLvhQTGf0MYymEJjj8QK', 1576262686, 0, 0),
(2, 'nikitabas83@gmail.com', 'Акіменко', '$2y$10$PXD/WPZPW4ppykXj8u7qJuUwWSmGfjU4JF04ixInLcWHoWSC4ZnZa', 1576264473, 0, 0),
(3, 'yatskovaiya2004@gmail.com ', 'Ия', '$2y$10$ApZHZWRM7DHXfmvEzKFRgOMBofKWSvjGr2ThIpwB8YCSnZtt1vfha', 1576264752, 0, 0),
(4, 'mihanamaster13@gmail.com', 'Michael', '$2y$10$Q2LIItBwGbptHG57oyvUD.8erVPFqWsy0gPrVKMOOfG9o9iQLpFgS', 1576268376, 0, 0),
(12, 'Zl0ta@gmail.com', 'test', '$2y$10$IV6jhAS/yxpEvaFCYqmdouUrATBsZEmG7kc7QOw6YPNVQv80YKBgi', 1576344980, 0, 0),
(13, '222', 'test', '$2y$10$bC0BncohnKMBlEEnMAHjBuxfoCKyv7bnm5lSrewfo0YXkz4lOeHW2', 1576366051, 0, 0),
(14, '111', '111', '$2y$10$2/KmG.TVnbM9jhIbOspIrOm4qVig4p1NL.OZuF9ID7R3agHWCPcmq', 1576369410, 0, 0),
(15, '1', 'Григо́рій Са́вич Сковорода́', '$2y$10$yTfo/Wu0g1koStt.PP.dwuFpgdsikC1dHBat0j6yvV./I12w2Yoyy', 1576625865, 7, 2),
(18, '2', '2', '$2y$10$YAybz48qfRCVacEnhtNbQuJUtxgUqALEJsKRHnEC0KrCXKjmnFM1G', 1576626148, 0, 0),
(19, '3', '3', '$2y$10$N5lovIyAwEfoC21V0M9ePOws4aGG6/pv5SLvhQTGf0MYymEJjj8QK', 1576626172, 0, 0),
(20, '4', '4', '$2y$10$kqGo6Yh/fYK11OH4huJHWOzog1KhfCjikxz9upMP1K4Od5AqovArG', 1576638754, 0, 0),
(21, 'qaz', 'qaz', '$2y$10$ECGkDVAWzXc.bRpGFnrgT.Iwq3BfbCQgtAgfIWJJyXiAVg1CoTrmm', 1578779814, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wishes`
--

CREATE TABLE `wishes` (
  `id` int(11) NOT NULL,
  `wish_id` int(11) NOT NULL,
  `wish_text` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wish_status` int(11) NOT NULL DEFAULT 0 COMMENT '0-100 - процент виконання',
  `add_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wishes`
--

INSERT INTO `wishes` (`id`, `wish_id`, `wish_text`, `wish_status`, `add_date`) VALUES
(9, 0, 'Тестове бажання №2', 0, '2020-02-02 11:21:37'),
(10, 0, 'ddasdasd', 0, '2020-02-02 11:21:37'),
(10, 1, 'asdasdas', 0, '2020-02-02 11:21:37'),
(10, 2, 'adasd asdasda adas da', 0, '2020-02-02 11:21:37'),
(1, 0, 'nongoer vrjnerjbnro rkntrklnrklr rejknrjnerngel erjkgnerjknerjk \r\nljbntbtrkbnl jbrjkhjfrbr tbkgnbrjk tr rjkbntrj rbjbrjnrivjd;oczn cjkbfdhgerufejiowv frt ujkyijotyhm nrjtiiorbn \n \n nongoer vrjnerjbnro rkntrklnrklr rejknrjnerngel erjkgnerjknerjk \r\nljbntbtrkbnl jbrjkhjfrbr tbkgnbrjk tr rjkbntrj rbjbrjnrivjd;oczn cjkbfdhgerufejiowv frt ujkyijotyhm nrjtiiorbn', 0, '2020-02-02 11:21:37'),
(1, 1, 'Овоуткт', 0, '2020-02-02 11:21:37'),
(15, 1, 'Хочу щоб всі були вільними', 77, '2020-02-02 11:21:37'),
(15, 0, 'Хочу бути вільним', 100, '2000-01-01 09:06:00'),
(1, 2, '\r\n         Я хочу придбати якусь якісну і цікаву книжку з економіки. У кого є зайва?)', 0, '2020-02-07 17:15:11'),
(15, 2, 'Мрію про велике.\r\nХочу написати козака Енея.', 0, '2020-02-10 00:07:25'),
(15, 4, 'wwwwwwwwwwwwwwwwwwwwwwww\r\nwwwwwwwwwwwwwwwwwwwwwwwww\r\nwwwwwwwwwwwwwwwwww', 50, '2020-02-10 00:32:36');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `pro_sebya`
--
ALTER TABLE `pro_sebya`
  ADD UNIQUE KEY `id` (`id`,`pro_sebya`) USING HASH;

--
-- Индексы таблицы `u_data`
--
ALTER TABLE `u_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- Индексы таблицы `u_data_old`
--
ALTER TABLE `u_data_old`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `u_data`
--
ALTER TABLE `u_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `u_data_old`
--
ALTER TABLE `u_data_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
