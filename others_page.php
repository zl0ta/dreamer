<?php
    session_start();
    $UID = $_SESSION['id'];
    if (!isset($UID)) header("Location: log_in.php");
?>
<!DOCTYPE html>
<head>
  <title>Інші користувачі</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial_scale=1.0">   <!--  адаптивность к мобильным устройствам-->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="css/based.css?v=0">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

</head>
<body style="width: 98%; margin: 0 auto;">
  <?php echo file_get_contents("menu.php"); ?>

  <div class="table_1"  style="margin-right: 15px; margin-left: 15px;" >
    <h2 style="padding-left: 10px;">Реєстр мрій користувачів</h2>

    <table class="display table table-striped table-bordered table-hover table-primary" id="dataTables-others" style="width:100%" cellpadding="15" cellspacing="0" >
        <thead>
            <tr>
                <th><H3 class="text-center" style="color: #00a8a8">&#9733;</H3></th>
                <th>Дата реєстрації бажання</th>
                <th>Бажання</th>
                <th>Ім'я користувача</th>
                <th>Аватар</th>
            </tr>
        </thead>
        <tbody>
             <?php require 'php/others_.php';?>
        </tbody>
    </table>





  </div>
  <script>
      $(document).ready(function() {
            // Setup - add a text input to each footer cell
            $('#dataTables-others thead tr').clone(true).appendTo( '#dataTables-others thead' );
            $('#dataTables-others thead tr:eq(1) th').each( function (i) {
                var title = $(this).text();
                $(this).css({"background-color":"#ffb145"});
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            var table = $('#dataTables-others').DataTable( {
              orderCellsTop: true,
              fixedHeader: true
            } );
      } );
  </script>
</body>
</html>
